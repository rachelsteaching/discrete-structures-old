\input{BASE-HEAD}
\newcommand{\laClass}       {Discrete Structures 1}
\newcommand{\laSemester}    {}
\newcommand{\laChapter}     {Logic: Quantifiers}
\newcommand{\laType}        {exercise}
\newcommand{\laPoints}      {5}
\newcommand{\laTitle}       {Logic: Quantifiers}
\newcommand{\laDate}        {}
\setcounter{chapter}{5}
\setcounter{section}{1}
\addtocounter{section}{-1}
\newcounter{question}

\toggletrue{answerkey}
%\togglefalse{answerkey}

\input{BASE-HEADER}

\input{BASE-INSTRUCTIONS-EXERCISE}

\notonkey{
% ASSIGNMENT ------------------------------------ %

    \section*{\laTitle}

    \subsection*{Predicates}

    \begin{intro}{Predicates\\}
        \large
        In mathematical logic, a \textbf{predicate} is commonly understood to be a Boolean-valued function.
        \footnote{From https://en.wikipedia.org/wiki/Predicate\_(mathematical\_logic)}

        We write a predicate as a function, such as $P(x)$, for example:

        \begin{center}
            $P(x)$ is the predicate, ``x is less than 2".
        \end{center}

        Once some value is plugged in for $x$, the result is a proposition -
        something either unambiguously \textbf{true} or \textbf{false},
        but until we have some input for $x$, we don't know whether it
        is true or false.

        \begin{center}
            $P(0)$ = true \tab $P(2)$ = false \tab $P(10)$ = false
        \end{center}

        Additionally, predicates can also be combined with the logical
        operators AND $\land$ OR $\lor$ and NOT $\neg$.
        \normalsize
    \end{intro}

    \newpage
    \stepcounter{question}
    \begin{questionNOGRADE}{\thequestion}
        For the following predicates given, plug in \textbf{2, 23, -5, and 15}
        as inputs and write out whether the result is true or false.
        
        \begin{enumerate}
            \item[a.]   $P(x)$ is the predicate ``$x > 15$"
                        \begin{itemize}
                            \item   $P(2) = $
                            \item   $P(23) = $
                            \item   $P(-5) = $
                            \item   $P(15) = $
                        \end{itemize}
            \item[b.]   $Q(x)$ is the predicate ``$x \leq 15$"
                        \begin{itemize}
                            \item   $P(2) = $
                            \item   $P(23) = $
                            \item   $P(-5) = $
                            \item   $P(15) = $
                        \end{itemize}
            \item[c.]   $R(x)$ is the predicate ``$(x > 5) \land (x < 20)$"
                        \begin{itemize}
                            \item   $P(2) = $
                            \item   $P(23) = $
                            \item   $P(-5) = $
                            \item   $P(15) = $
                        \end{itemize}
        \end{enumerate}
    \end{questionNOGRADE}

    \newpage

    \begin{intro}{Domain\\}
        \large
        When we're working with predicates, we will also define the domain.
        The \textbf{domain} is the set of all possible inputs for our predicate.
        In other words, $x$ must be chosen from the domain.
        \normalsize
    \end{intro}

    \stepcounter{question}
    \begin{questionNOGRADE}{\thequestion}
        For the following predicates and domains given, specify
        whether the predicate is true for \textbf{all members of the domain},
        \textbf{some members of the domain}, or \textbf{no members of the domain.}
        
        \begin{enumerate}
            \item[a.]   $P(x)$ is the predicate ``$x > 15$", the domain is \{10, 12, 14, 16, 18\}.
                        ~\\
                        \Square \ True for all \tab \Square \ True for some \tab \Square \ True for none \\
            
            \item[b.] $Q(x)$ is the predicate ``$x \leq 15$", the domain is \{0, 1, 2, 3\}.
                        ~\\
                        \Square \ True for all \tab \Square \ True for some \tab \Square \ True for none \\
                        
            \item[c.] $R(x)$ is the predicate ``$(x > 5) \land (x < 20)$", the domain is \{0, 1, 2\}.
                        ~\\
                        \Square \ True for all \tab \Square \ True for some \tab \Square \ True for none \\
                        
            \item[d.] $S(x)$ is the predicate ``$(x > 1) \land (x < 5)$", domain is \{2, 3, 4\}.
                        ~\\
                        \Square \ True for all \tab \Square \ True for some \tab \Square \ True for none \\
        \end{enumerate}
    \end{questionNOGRADE}

    \newpage

    \subsection*{Quantifiers}

    \begin{intro}{Quantifiers\\}
        \large
        Symbolically, we can specify that the input of our predicate, $x$,
        belongs in some domain set $D$ with the notation: $x \in D$.
        This is read as, ``$x$ exists in the domain $D$."

        ~\\
        Additionally, we can also specify whether a predicate is true
        \textbf{for all inputs $x$ from the domain $D$} using the ``for all"
        symbol $\forall$, or we can specify that the predicate is true
        \textbf{for \textit{some} inputs $x$ from the domain $D$} using
        the ``there exists" symbol $\exists$.{}

        \paragraph{Example:} Rewrite the predicate symbolically. ~\\
            $P(x)$ is ``$x > 15$", the domain D is \{16, 17, 18\}.
            Here we can see that all inputs from the domain will
            result in the predicate evaluating to true, so we can write:

            \begin{center}
                $ \forall x \in D, P(x) $ (``For all x in D, x is greater than 15.")
            \end{center}

        \begin{itemize}
            \item The symbol $\in$ (``in") indicates membership in a set.
            \item The symbol $\forall$ (``for all") means "for all", or "every".
            \item The symbol $\exists$ (``there exists") means "there is (at least one)", or "there exists (at least one)".
            \item The symbols $\forall$ and $\exists$ are called \textbf{quantifiers}. When used
                with predicates, the statement is called a \textbf{quantified predicate}.
        \end{itemize}
        \normalsize
    \end{intro}

    \newpage
    \stepcounter{question}
    \begin{questionNOGRADE}{\thequestion}
        For the following predicates, rewrite the sentence symbolically,
        as in the example above.
        Use either $\forall$ or $\exists$, based on whether the
        predicate is true for the domain given.
        
        \paragraph{Hint:}
            If a predicate P(x) is false for all elements in the domain, you
            can phrase it as: ``$\forall x \in D, \neg P(x)$".
        
        \begin{enumerate}
            \item[a.]   $P(x)$ is the predicate ``$x > 15$", the domain is \{10, 12, 14, 16, 18\}.
                        \vspace{2cm}
            \item[b.]   $Q(x)$ is the predicate ``$x \leq 15$", the domain is \{0, 1, 2, 3\}.
                        \vspace{2cm}
            \item[c.]   $R(x)$ is the predicate ``$(x > 5) \land (x < 20)$", the domain is \{0, 1, 2\}.
                        \vspace{2cm}
            \item[d.]   $S(x)$ is the predicate ``$(x > 1) \land (x < 5)$", domain is \{2, 3, 4\}.
                        \vspace{2cm}
        \end{enumerate}
    \end{questionNOGRADE}

    \newpage

    \stepcounter{question}
    \begin{questionNOGRADE}{\thequestion}
        For the following predicates, rewrite the sentence symbolically
        using the domain D = \{ 3, 4, 5, 10, 20, 25 \}.
        Make sure to define your predicates (state that ``$P(x)$ is the predicate...",
        and afterwards specify whether the quantified predicate is true or false.
        (If it states ``there exists" but none exist, then the quantified predicate is false.)
        
        \begin{enumerate}
            \item[a.]   There exists some $m$ that is a member of $D$, such that $m \geq 3$.
                        \vspace{3cm}
                        
            \item[b.]   There exists some element in $D$ that is negative.
                        \vspace{3cm}
                        
            \item[c.]   There is (at least one) $k$ in the set $D$ with the property that $k^{2}$ is also in the set $D$.
                        \begin{hint}{Hint}
                            How can you specify the predicate, ``$k^{2}$ is in the set $D$" symbolically?
                        \end{hint}
                        \vspace{3cm}
        \end{enumerate}
    \end{questionNOGRADE}

    \newpage
    \subsection*{Negating quantifiers}

        \begin{intro}{Negating quantifiers}
            \large            
            For any predicates $P$ and $Q$ over a domain $D$,
            \begin{itemize}
                \item The negation of $\forall x \in D, P(x)$ is $\exists x \in D, \neg P(x)$.
                \item The negation of $\exists x \in D, P(x)$ is $\forall x \in D, \neg P(x)$.
            \end{itemize}

            When negating a predicate that uses an equal sign, the negation would be ``not equals".

            \paragraph{Example 1:} Negate $\forall x \in \mathbb{Z}, x > 0$.
            \small ~\\ \textit{(For all integers $x$, $x$ is greater than 0.)} \large
            \begin{enumerate}
                \item   $\neg( \forall x \in \mathbb{Z}, x > 0 )$  
                \item   $\equiv \neg( \forall x \in \mathbb{Z} )$, $\neg( x > 0 ) $
                \item   $\equiv \exists x \in \mathbb{Z}, x \leq 0$
                        ~\\ \footnotesize \textit{(There exists some integer $x$ such that $x$ is less than or equal to than 0.)} \large
            \end{enumerate}

            In this case, the original statement ``$\forall x \in \mathbb{Z}, x > 0$'' was incorrect, and the negation ``$\exists x \in \mathbb{Z}, x \leq 0$'' is valid.
        \end{intro}

    \newpage

    \stepcounter{question}
    \begin{questionNOGRADE}{\thequestion}
        Write the negation of each of these statements. Simplify as much as possible.
        Identify whether the original statement or the negation is true.
        
        \begin{enumerate}
            \item[a.]   $\forall x \in \mathbb{Z},$ \tab[0.2cm]  \tab[0.2cm] $P(x)$.    ~\\ $P(x)$ is ``$2x$ is even''.
                        \vspace{5cm}
            \item[b.]   $\exists x \in \mathbb{N},$ \tab[0.2cm]  \tab[0.2cm] $Q(x)$.    ~\\ $Q(x)$ is ``$x \cdot x < 0$''.
                        \vspace{5cm}
            \item[c.]   $\forall x \in \mathbb{N},$ \tab[0.2cm]  \tab[0.2cm] $R(x)$.    ~\\ $R(x)$ is ``$x \in \mathbb{Z}$''.
                        \vspace{5cm}
        \end{enumerate}
    \end{questionNOGRADE}

    \newpage
    \stepcounter{question}
    \begin{questionNOGRADE}{\thequestion}
        Which elements of the set $D$ = \{2, 4, 6, 8, 10, 12\} make the
        \textbf{negation} of each of these predicates true? List the numbers from $D$ that make the \underline{negation} true.
        
        \begin{enumerate}
            \item[a.]   $Q(n)$ is the predicate, ``$n > 10$".
                        \vspace{3cm}
            \item[b.]   $R(n)$ is the predicate, ``$n$ is even".
                        \vspace{3cm}
            \item[c.]   $S(n)$ is the predicate, ``$n^{2} < 1$".
                        \vspace{3cm}
            \item[d.]   $T(n)$ is the predicate, ``$n-2$ is an element of $D$".
                        \vspace{3cm}
        \end{enumerate}
    \end{questionNOGRADE}


}{
% KEY ------------------------------------ %

\begin{enumerate}
    \item   \small
        \begin{itemize}
            \item[a.]      $P(2):$  False  \tab
                            $P(23):$ True   \tab
                            $P(-5):$ False  \tab
                            $P(15):$ False

            \item[b.]      $Q(2):$  True   \tab
                            $Q(23):$ False  \tab
                            $Q(-5):$ True   \tab
                            $Q(15):$ True   

            \item[c.]      $R(2):$  False  \tab
                            $R(23):$ False  \tab
                            $R(-5):$ False  \tab
                            $R(15):$ True
        \end{itemize}

    \item
        \begin{itemize}
            \item[a.]      True for some
            \item[b.]      True for all
            \item[c.]      True for none
            \item[d.]      True for all
        \end{itemize}

    \item
        \begin{itemize}
            \item[a.]      $\exists x \in D, P(x)$
            \item[b.]      $\forall x \in D, Q(x)$
            \item[c.]      $\forall x \in D, \neg R(x)$
            \item[d.]      $\forall x \in D, S(x)$            
        \end{itemize}

    \item
        \begin{itemize}
            \item[a.]      $\exists k \in D, P(x)$, where $P(x)$ is the predicate ``$k^{2} \in D$".
            \item[b.]      $\exists m \in D, Q(x)$, where $Q(m)$ is the predicate ``$m \geq 3$".
        \end{itemize}

    \item
        \begin{itemize}
            \item[a.]   $\neg ( \forall x \in \mathbb{Z}, 2x$ is even $) \tab \equiv \tab
                            \exists x \in \mathbb{Z}, 2x $ is NOT even. ~\\
                            Original statement is true.
            \item[b.]   $\neg ( \exists x \in \mathbb{N}, x \cdot x < 0 ) \tab \equiv \tab
                            \forall x \in \mathbb{N}, x \cdot x \geq 0$ ~\\
                            Negation is true.
            \item[c.]   $\neg ( \forall x \in \mathbb{N}, x \in \mathbb{Z} ) \tab \equiv \tab
                            \exists x \in \mathbb{N}, x \not\in \mathbb{Z}$ ~\\
                            Original is true.
        \end{itemize}
        
    \item
        \begin{itemize}
            \item[a.]      $\neg Q(n)$ is $n \leq 10$, so \{2, 4, 6, 8, and 10\}.
            \item[b.]      $\neg R(n)$ is $n$ is odd, so NONE.
            \item[c.]      $\neg S(n)$ is $n^{2} \geq 1$, so ALL.
            \item[d.]      $\neg T(n)$ is $n-1$ is not an element of $D$, so \{2\}.
        \end{itemize}
\end{enumerate}

}

\input{BASE-FOOT}
