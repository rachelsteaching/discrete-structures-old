\input{BASE-HEAD}
\newcommand{\laClass}       {Discrete Structures 1}
\newcommand{\laSemester}    {}
\newcommand{\laChapter}     {Logic: Implications}
\newcommand{\laType}        {exercise}
\newcommand{\laPoints}      {5}
\newcommand{\laTitle}       {Logic: Implications (conditional propositions)}
\newcommand{\laDate}        {}
\setcounter{chapter}{1}
\setcounter{section}{1}
\addtocounter{section}{-1}
\newcounter{question}

\toggletrue{answerkey}
%\togglefalse{answerkey}

\input{BASE-HEADER}

\input{BASE-INSTRUCTIONS-EXERCISE}

\notonkey{
% ASSIGNMENT ------------------------------------ %

    \section*{\laTitle}

    \subsection*{Implications}

        \begin{intro}{Implications} ~\\
            A statement like ``if $p$ is true, then $q$ is true" is known
            as an implication. It can be written symbolically as $p \to q$,
            and read as ``p implies q" or ``if p, then q".
            For the implication $p \to q$, $p$ is the \textbf{hypothesis}
            and $q$ is the \textbf{conclusion}.

            \paragraph{Example:} Write the following statement symbolically,
                defining the propositional variable:

                \begin{center}
                    ``If it is Kate's birthday, then Kate will get a cake."
                \end{center}
                ~\\
                I will define two propositional variables:

                $b$ is ``It is Kate's birthday" \tab $c$ is ``Kate will get a cake."

                ~\\
                And then I can write it symbolically: $b \to c$.
        \end{intro}

    \newpage

    \stepcounter{question}
    \begin{questionNOGRADE}{\thequestion}
        For the following statements, create two
        \textbf{propositional variables}, assign them values (from
        the original statement), and then write it symbolically.

        \begin{enumerate}
            \item[a.] IF you don't play, THEN you can't win!
                        ~\\ \tab Variables:
                        \vspace{1cm}
                        ~\\ \tab Symbolic statement:
                        \vspace{1cm}
            \item[b.] IF your friends don't dance, THEN they aren't friends of mine!
                        ~\\ \tab Variables:
                        \vspace{1cm}
                        ~\\ \tab Symbolic statement:
                        \vspace{1cm}
            \item[c.] IF Timmy's age is over 8 AND Timmy's age is less than 13, THEN Timmy gets Tween-priced movie tickets.
                        ~\\ \tab Variables:
                        \vspace{1cm}
                        ~\\ \tab Symbolic statement:
                        \vspace{1cm}
            \item[d.] IF I get enough sleep, OR I drink coffee, THEN I can go to work.
                        ~\\ \tab Variables:
                        \vspace{1cm}
                        ~\\ \tab Symbolic statement:
                        \vspace{1cm}
        \end{enumerate}        
    \end{questionNOGRADE}

    \newpage

        \begin{intro}{Truth for implications}
            With an implication, ``if \textbf{hypothesis}, then \textbf{conclusion}",
            the truth table will look a bit different than what you've done so far.
            Make sure to take notice because it is easy to make an error when it
            comes to implication truth tables!

            For an implication to be logically FALSE, it must be the case that
            the \textbf{hypothesis is true, but the conclusion is false}. In
            all other cases, the implication results to \textbf{true}.

            \begin{center}
                \begin{tabular}{| c | c | c | c |}
                    \hline{}
                    $p$ & $q$ & & $p \to q$
                    \\ \hline
                    T & T & & T
                    \\ \hline

                    T & F & & F
                    \\ \hline

                    F & T & & T
                    \\ \hline

                    F & F & & T
                    \\ \hline
                \end{tabular}
            \end{center}

            Seems weird? Think of it this way: The only FALSE result is if
            the \textbf{hypothesis} is true, but the \textbf{conclusion}
            ends up being false. In science, it would mean our hypothesis
            has been disproven. If, however, the \textbf{hypothesis}
            is false, we can't really say anything about the conclusion
            (again thinking about science experiments) - it is \textbf{true by default.}
        \end{intro}

    \stepcounter{question}
    \begin{questionNOGRADE}{\thequestion}
        Complete the truth tables for the expression $\neg( p \to q )$
        ~\\
        \large
        \begin{tabular}{c c | c | c p{6cm} }
            $p$ & $q$ & $p \to q$ & $\neg( p \to q )$
            \\ \hline
            & & & \\
            T & T  & & & \\
            & & & \\
            T & F  & & \\
            & & & \\
            F & T  & & \\
            & & & \\
            F & F  & & \\
            & & & \\
        \end{tabular}
        \normalsize
    \end{questionNOGRADE}

    \newpage

    \stepcounter{question}
    \begin{questionNOGRADE}{\thequestion}
        Complete the truth tables for the expression $(p \land q) \to r$
        ~\\
        \Large
        \begin{tabular}{c c c | c | c p{5cm} }
            $p$ & $q$ & $r$ & $p \land q$ & $(p \land q) \to r$
            \\ \hline
            T & T & T & T & & \\
            T & T & F & T  & \\
            T & F & T & F & \\
            T & F & F & F & \\
            F & T & T & F & \\
            F & T & F & F & \\
            F & F & T & F & \\
            F & F & F & F & \\
        \end{tabular}
        \normalsize
    \end{questionNOGRADE}
    \vspace{1cm}
    
    \stepcounter{question}
    \begin{questionNOGRADE}{\thequestion}
        Complete the truth tables for the expression $p \to (q \lor r)$
        ~\\
        \Large
        \begin{tabular}{c c c | c | c p{5cm} }
            $p$ & $q$ & $r$ & $q \lor r$ &  $p \to (q \lor r)$
            \\ \hline
            T & T & T &  & & \\
            T & T & F &   & \\
            T & F & T &  & \\
            T & F & F &  & \\
            F & T & T &  & \\
            F & T & F &  & \\
            F & F & T &  & \\
            F & F & F &  & \\
        \end{tabular}
        \normalsize
    \end{questionNOGRADE}

    \newpage

    \subsection*{Negations of Implications}

    \begin{intro}{\ }
        The negation of the implication $p \to q$ is the statement
        $p \land (\neg q)$. We can see that these are equivalent
        with a truth table:

        \begin{center}
            \begin{tabular}{c c | c | c | c }
                $p$ & $q$ & $p \to q$ & $\neg( p \to q)$ & $p \land \neg q$
                \\ \hline
                T & T & T & F & F
                \\
                T & F & F & T & T
                \\
                F & T & T & F & F
                \\
                F & F & T & F & F
            \end{tabular}
        \end{center}

        It is important to remember that \textbf{the negation of an
        implication is \underline{not} also an implication!}

        \paragraph{Example:} We're going to do science. My hypothesis is
        that ``a watched pot never boils", so let's turn this into an
        implication.

        \begin{center}
            Hypothesis $p$: ``You watch a pot." \\
            Conclusion $q$: ``The pot never boils." \\
            $p \to q$: If you watch a pot, then the pot never boils.
        \end{center}

        There are four different things that can happen:

        \begin{enumerate}            
            \item If we watch the pot ($p$), and it never boils ($q$), then our implication is true:
                We've proven it. Our implication is \textbf{true}.
            \item If we watch the pot ($p$), and it DOES boil ($\neg q$), then our hypothesis has been disproven:
                we did the experiment, and our conclusion was wrong, so the implication is \textbf{false}.
            \item If we don't watch the pot ($\neg p$), and it never boils ($q$), while our conclusion
                may be true, we didn't even bother to follow through with our hypothesis; we failed at science.
                We can't prove or disprove our original hypothesis. In this case, the implication is
                still \textbf{true}, because we haven't disproven it.
            \item If we don't watch the pot ($\neg p$), and it DOES boil ($\neg q$), same as above,
                we aren't performing our hypothesis, so this doesn't prove or disprove anything;
                the implication is still \textbf{true}.
        \end{enumerate}

        If the experiment FAILS (results in false), then it means $p \land \neg q$.
    \end{intro}

    \newpage

    \stepcounter{question}
    \begin{questionNOGRADE}{\thequestion}
        For each of the following statements, write out the negation
        both symbolically and in English.


        \begin{enumerate}
            \item[a.] $i$: If you go to New Delhi, $j$: You will see the Jantar Mantar. \\
                $i \to j$: If you go to New Delhi, then you will see the Jantar Mantar.
                \vspace{4cm}
                
            \item[b.]
                $j$: Jessica gets chocolate,
                $c$: Jessica gets cake, \\
                $b$: Jessica has a happy birthday. \\
                $(j \lor c) \to b$: If Jessica gets chocolate or Jessica gets cake, then Jessica has a happy birthday.
                \vspace{4cm}
                
            \item[c.]
                $p$: You have a group project, $w$: You do all the work, \\
                $s$: Someone else does all the work. \\
                $p \to (w \lor s)$: If you have a group project, then either you do all the work,
                    or someone else does all the work.
                \vspace{2cm}
        \end{enumerate}        
    \end{questionNOGRADE}

}{
% KEY ------------------------------------ %
    \footnotesize
    \begin{enumerate}
        \item
            \begin{enumerate}
                \item[a.]   $p$: You play, $w$: You win. ~\\ $\neg p \to \neg w$
                \item[b.]   $d$: Your friends dance, $f$: your friends are my friends ~\\ $\neg d \to \neg f$
                \item[c.]   $e$: Timmy's age is over 8, $t$: Timmy's age is less than 13, ~\\$m$: Timmy gets tween priced movie tickets ~\\
                            $(e \land t) \to m$
                \item[d.]   $s$: I get enough sleep, $c$: I drink coffee, $w$: I can go to work. ~\\ $(s \lor c) \to w$
            \end{enumerate}
        \item   ~\\
            \begin{tabular}{c c | c | c c }
                $p$ & $q$ & $p \to q$ & $\neg( p \to q )$
                \\ \hline
                T & T  & T & F & \\
                T & F  & F & T \\
                F & T  & T & F \\
                F & F  & T & F \\
            \end{tabular}
        \item ~\\
            \begin{tabular}{c c c | c | c c }
                $p$ & $q$ & $r$ & $p \land q$ & $(p \land q) \to r$
                \\ \hline
                T & T & T & T & T & \\
                T & T & F & T & F \\
                T & F & T & F & T \\
                T & F & F & F & T \\
                F & T & T & F & T \\
                F & T & F & F & T \\
                F & F & T & F & T \\
                F & F & F & F & T \\
            \end{tabular}
        \item ~\\
            \begin{tabular}{c c c | c | c c }
                $p$ & $q$ & $r$ & $q \lor r$ &  $p \to (q \lor r)$
                \\ \hline
                T & T & T & T & T  & \\
                T & T & F & T & T \\
                T & F & T & T & T \\
                T & F & F & F & F \\
                F & T & T & T & T \\
                F & T & F & T & T \\
                F & F & T & T & T \\
                F & F & F & F & T \\
            \end{tabular}
        \item
            \begin{enumerate}
                \item[a.]   $\neg (i \to j) \equiv i \land \neg j$ ~\\
                            You go to New Delhi and you don't see the Jantar Mantar.
                    
                \item[b.]   $\neg ( (j \lor c) \to b ) \equiv (j \lor c) \land \neg b$ ~\\
                            Jessica gets chocolate and cake, and Jessica does not have a happy birthday.
                    
                \item[c.]   $\neg ( p \to (w \lor s) ) \equiv p \land \neg (w \lor s) \equiv p \land \neg w \land \neg s$ ~\\
                            You have a group project, and you don't do all the work, and someone else doesn't do all the work.
            \end{enumerate}   
    \end{enumerate}

}

\input{BASE-FOOT}
