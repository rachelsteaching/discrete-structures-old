\input{BASE-HEAD}
\newcommand{\laClass}       {Discrete Structures 1}
\newcommand{\laSemester}    {}
\newcommand{\laChapter}     {Logic: Proposition basics}
\newcommand{\laType}        {exercise}
\newcommand{\laPoints}      {5}
\newcommand{\laTitle}       {Logic: Proposition basics}
\newcommand{\laDate}        {}
\setcounter{chapter}{1}
\setcounter{section}{1}
\addtocounter{section}{-1}
\newcounter{question}

\toggletrue{answerkey}
%\togglefalse{answerkey}

\input{BASE-HEADER}

\input{BASE-INSTRUCTIONS-EXERCISE}

\notonkey{
% ASSIGNMENT ------------------------------------ %

    \section*{\laTitle}

    \subsection*{Propositional Logic}

    \begin{intro}{\ }
        A \textbf{proposition} is a statement which has truth value: it is either true (T) or false (F).
        \footnote{From https://en.wikibooks.org/wiki/Discrete\_Mathematics/Logic}

        The statement doesn't have to necessarily be TRUE, it could also
        be FALSE, but it has to be unambiguously so. ~\\

        \begin{tabular}{ | l | l | l | }
            \hline
            \textbf{Statement} & \textbf{Proposition?} & \textbf{Result?}
            \\ \hline
            2 + 3 = 5 & Yes & True
            \\ \hline
            2 + 2 = 5 & Yes & False
            \\ \hline
            This class has 30 students & Yes & False\footnote{Probably :)}
            \\ \hline
            Is the movie good? & No &
            \\ \hline
        \end{tabular}
    \end{intro}

    \stepcounter{question}
    \begin{questionNOGRADE}{\thequestion}
        \small For the following, mark whether the statement is a \textbf{proposition} and,
        if it is, mark whether it is \textbf{true} or \textbf{false}.

        \begin{center}
            \begin{tabular}{l l | c | c}
                & \textbf{statement} & \textbf{is proposition?} & \textbf{is true/false?}
                \\ \hline
                a. & 10 + 20 = 30
                & & \\ \hline

                b. & 2 times an integer is always even.
                & & \\ \hline

                c. & Is $x < 5$?
                & & \\ \hline

                d. & $5 + 1$ is odd.
                & & \\
            \end{tabular}
        \end{center}
    \end{questionNOGRADE}
    \normalsize

    \newpage

    \subsection{Compound Propositions}

    \begin{intro}{\ }
        We can also create a \textbf{compound proposition} using the logical operators
        for AND $\land$, OR $\lor$, and NOT $\neg$. When we're writing out
        a compound proposition, we will usually assign each proposition a
        \textbf{propositional variable}.

        ~\\ If we have the propositions
        $p$ is the proposition ``Paul is taking discrete math",
        $q$ is the proposition, ``Paul has a calculator",
        then we can build the compound propositions like:
        \begin{itemize}
            \item[] $p \land q$: Paul is taking discrete math and Paul has a calculator
            \item[] $p \lor q$: Paul is taking discrete math or Paul has a calculator
            \item[] $\neg p$: Paul is NOT taking discrete math
            \item[] $\neg q$: Paul does NOT have a calculator
        \end{itemize}

        The result of a compound proposition depends on the value of
        each proposition it is made up of:

        \begin{enumerate}
            \item A compound proposition $a \land b \land c$ is \textbf{only true}
                if all propositions are true; it will be \textbf{false} if
                one or more of the propositions is false.
            \item A compound proposition $a \lor b \lor c$ is \textbf{true}
                if one or more of the propositions is true; it is \textbf{only false}
                if all propositions are false.
            \item A compound proposition $\neg a$ is \textbf{true} only
                if the proposition is false; it is only \textbf{false} if
                the proposition is true.
        \end{enumerate}

    \end{intro}

    \newpage

    \stepcounter{question}
    \begin{questionNOGRADE}{\thequestion}
        Given the following compound propositions and proposition values,
        write out whether the full compound proposition is \textbf{true} or \textbf{false}.

        \large

        \paragraph{a. a AND b:} ~\\

        \begin{tabular}{ | l  c | c | p{4cm} | }
            \hline
            & \textbf{Compound} & \textbf{Values} & \textbf{Result}
            \\ \hline

            a. &        $a \land b$ &       $a = true, b = true$ &       TRUE   \\ \hline
            b. &        $a \land b$ &       $a = true, b = false$ &       FALSE   \\ \hline
            c. &        $a \land b$ &       $a = false, b = true$ &       FALSE   \\ \hline
            d. &        $a \land b$ &       $a = false, b = false$ &       FALSE   \\ \hline
        \end{tabular}

        \paragraph{b. a OR b:} ~\\

        \begin{tabular}{ | l  c | c | p{4cm} | }
            \hline
            & \textbf{Compound} & \textbf{Values} & \textbf{Result}
            \\ \hline

            e. &        $a \lor b$ &       $a = true, b = true$ &     \\ \hline
            f. &        $a \lor b$ &       $a = true, b = false$ &    \\ \hline
            g. &        $a \lor b$ &       $a = false, b = true$ &    \\ \hline
            h. &        $a \lor b$ &       $a = false, b = false$ &   \\ \hline
        \end{tabular}

        \paragraph{c. Combinations:} ~\\

        \begin{tabular}{ | l  c | c | p{4cm} | }
            \hline
            & \textbf{Compound} & \textbf{Values} & \textbf{Result}
            \\ \hline

            i. &        $a \land \neg b$ &       $a = true, b = false$ &   \\ \hline
            j. &        $a \lor \neg b$ &        $a = false, b = true$ &   \\ \hline

            k. &        $\neg a \land b$ &       $a = false, b = true$ &   \\ \hline
            l. &        $\neg a \lor b$ &        $a = false, b = false$ &  \\ \hline
        \end{tabular}

    \end{questionNOGRADE}
    \normalsize

    \newpage

    \stepcounter{question}
    \begin{questionNOGRADE}{\thequestion}
        For the following propositions...
        
        \begin{center}
            $m$: There is milk in the fridge \tab $c$: There is cheese in the fridge
        \end{center}

        ~\\
        ...``Translate" the following English statements
        into compound propositions. Use parentheses when mixing $\land$ and $\lor$ and $\neg$ operators.

        
        \begin{enumerate}
            \item[a.]   There is milk in the fridge, OR there is cheese in the fridge.
                        \vspace{2cm}
            \item[b.]   It is not true that: milk AND cheese are in the fridge.
                        \vspace{2cm}
            \item[c.]   There is milk in the fridge, AND there is NOT cheese in the fridge.
                        \vspace{2cm}
            \item[d.]   There is cheese in the fridge, AND there is NOT milk in the fridge.
                        \vspace{2cm}
        \end{enumerate}

    \end{questionNOGRADE}


    \newpage

    \subsection*{Truth Tables}

    \begin{intro}{\ }
        When we're working with compound propositional statements,
        the result of the compound depends on the true/false values
        of each proposition it is built up of.
        ~\\
        We can diagram out all possible states of a compound proposition
        by using a \textbf{truth table}. In a truth table, we list
        all propositional variables first on the left, as well as
        all possible combinations of their states, and then
        the compound statement's result on the right.

        ~\\
        \begin{center}
            \begin{tabular}{ l l l }

                \begin{tabular}{ | c | c || c | }
                    \hline
                    $p$ & $q$ & $p \land q$ \\ \hline
                    T & T & T \\ \hline
                    T & F & F \\ \hline
                    F & T & F \\ \hline
                    F & F & F \\ \hline

                \end{tabular}
                &

                \begin{tabular}{ | c | c || c | }
                    \hline
                    $p$ & $q$ & $p \lor q$ \\ \hline
                    T & T & T \\ \hline
                    T & F & T \\ \hline
                    F & T & T \\ \hline
                    F & F & F \\ \hline

                \end{tabular}
                &

                \begin{tabular}{ | c || c | }
                    \hline
                    $p$ & $\neg p$ \\ \hline
                    T & F \\ \hline
                    F & T \\ \hline

                \end{tabular}

            \end{tabular}
        \end{center}
    \end{intro}

    \stepcounter{question}
    \begin{questionNOGRADE}{\thequestion}
        Complete the following truth tables. \large

        \begin{enumerate}
            \item[a.] ~\\
                \begin{tabular}{ | p{1cm} | p{1cm} || p{2cm} | p{3cm} | }
                    \hline
                    $p$ & $q$ & $\neg q$ & $p \land \neg q$ \\ \hline
                    T & T & F & \\ \hline
                    T & F & T & \\ \hline
                    F & T &  & \\ \hline
                    F & F &  & \\ \hline
                \end{tabular} ~\\
                
            \item[b.] ~\\
                \begin{tabular}{ | p{1cm} | p{1cm} || p{2cm} | p{2cm} | p{3cm} | }
                    \hline
                    $p$ & $q$ & $\neg p$ & $\neg q$ & $\neg p \lor \neg q$ \\ \hline
                    T & T & & & \\ \hline
                    T & F & & & \\ \hline
                    F & T & & & \\ \hline
                    F & F & & & \\ \hline
                \end{tabular}
        \end{enumerate}
        \normalsize
    \end{questionNOGRADE}

    \newpage
    \begin{intro}{\ }
        When you're building out truth tables, there is a specific order
        you should write out the ``T" and ``F" states.
        Begin with all ``T" values first, and work your way down to
        all ``F" values first. As you go, change the right-most state
        from ``T" to ``F", working your way from right-to-left.

        ~\\
        So with two variables: \\ ``TT", ``TF", ``FT", ``FF".
    \end{intro}

    \stepcounter{question}
    \begin{questionNOGRADE}{\thequestion}
        Using the rules above, write out all the states for a
        truth table with three propositional variables. ~\\ \large

        \begin{center}
            \begin{tabular}{ | p{2cm} | p{2cm} | p{2cm} | }
                \hline
                $p$ & $q$ & $r$   \\ \hline
                T & T & T         \\ \hline
                T & T & F         \\ \hline
                T &  &          \\ \hline
                T & F & F         \\ \hline
                F &  &          \\ \hline
                F &  &          \\ \hline
                F &  &          \\ \hline
                F & F & F         \\ \hline
            \end{tabular}
        \end{center}
        \normalsize
    \end{questionNOGRADE}

    ~\\

    \newpage

    \begin{intro}{\ }
        Whenever the final columns of the truth tables for two propositions $p$ and $q$ are the same,
        we say that $p$ and $q$ are \textbf{logically equivalent}, and we write: $p \equiv q$.
        \footnote{From https://en.wikibooks.org/wiki/Discrete\_Mathematics/Logic}
    \end{intro}

    \stepcounter{question}
    \begin{questionNOGRADE}{\thequestion}
        Use a truth table to show that the compound propositions,

        \begin{center}
        $ \neg p \land \neg q $
        \tab and \tab
        $ \neg ( p \lor q ) $
        \end{center}
        are logically equivalent. The final two columns are
        the compound propositions above
        ~\\ \large
        \begin{center}
            %   p       q       !p              !q          p or q          !p and !q                   !(p or q)
            \begin{tabular}
                { c     | c     || c            | c         || c            || c                        || c         }
                $p$     & $q$   & $\neg p$      & $\neg q$  & $(p \lor q)$  & $ \neg p \land \neg q $   & $ \neg ( p \lor q ) $     \\ \hline
                &&&&&\\ % spacer
                T       & T     &               &                           &                           &                           \\
                &&&&&\\ \hline % spacer
                &&&&&\\ % spacer
                T       & F     &               &                           &                           &                           \\ 
                &&&&&\\ \hline % spacer
                &&&&&\\ % spacer
                F       & T     &               &                           &                           &                           \\
                &&&&&\\ \hline % spacer
                &&&&&\\ % spacer
                F       & F     &               &                           &                           &                           \\
                
            \end{tabular}
        \end{center}
        \normalsize
    \end{questionNOGRADE}

    \newpage

    
    \stepcounter{question}
    \begin{questionNOGRADE}{\thequestion}
        For Question 3, we had the propositions: ~\\
        \begin{center}
            $m$: There is milk in the fridge \tab
            $c$: There is cheese in the fridge
        \end{center}

        ~\\
        a. Build a statement for:

        \begin{center}
            \textit{ There is milk in the fridge, OR there is cheese in the fridge, \\
            BUT not both at the same time. }
        \end{center}

        \vspace{2cm}

        ~\\
        b. Validate your statement using the truth table:

        ~\\ 
        \begin{tabular}{c | c | | p{11cm}  }
            & & Your statement from (a)... (write in)
            \\ & & \\ $m$ & $c$ & \\ \hline
            & & \\ T & T & \\ & & \\ \hline
            & & \\ T & F & \\ & & \\ \hline
            & & \\ F & T & \\ & & \\ \hline
            & & \\ F & F & \\ & &
        \end{tabular}
        
    \end{questionNOGRADE}
}
{
% KEY ------------------------------------ %

    \begin{enumerate}
        \item
            \begin{tabular}{l l | c | c}
                & \textbf{statement} & \textbf{is proposition?} & \textbf{is true/false?}
                \\ \hline
                a. & 10 + 20 = 30
                & yes & true \\ \hline

                b. & 2 times an integer is always even.
                & yes & true \\ \hline

                c. & Is $x < 5$?
                & no & - \\ \hline

                d. & $5 + 1$ is odd.
                & yes & false \\
            \end{tabular}

        \item   
            \begin{itemize}
                \item[b.]    ~\\
                    \begin{tabular}{ | l  c | c | p{4cm} | }
                        \hline
                        & \textbf{Compound} & \textbf{Values} & \textbf{Result}
                        \\ \hline

                        e. &        $a \lor b$ &       $a = true, b = true$ &   true    \\ \hline
                        f. &        $a \lor b$ &       $a = true, b = false$ &  true   \\ \hline
                        g. &        $a \lor b$ &       $a = false, b = true$ &  true  \\ \hline
                        h. &        $a \lor b$ &       $a = false, b = false$ & false  \\ \hline
                    \end{tabular}
                \item[c.]    ~\\
                    \begin{tabular}{ | l  c | c | p{4cm} | }
                        \hline
                        & \textbf{Compound} & \textbf{Values} & \textbf{Result}
                        \\ \hline

                        i. &        $a \land \neg b$ &       $a = true, b = false$ & true  \\ \hline
                        j. &        $a \lor \neg b$ &        $a = false, b = true$ & false  \\ \hline

                        k. &        $\neg a \land b$ &       $a = false, b = true$ &  true \\ \hline
                        l. &        $\neg a \lor b$ &        $a = false, b = false$ & true \\ \hline
                    \end{tabular}
            \end{itemize}

        \item   
            \begin{itemize}
                \item[a.]   There is milk in the fridge, OR there is cheese in the fridge.
                \item[b.]   It is not true that: milk AND cheese are in the fridge.
                \item[c.]   There is milk in the fridge, OR there is cheese in the fridge, BUT not both at the same time. ~\\
                            \footnotesize \textit{(Hint: Use parentheses, and combine (a) and (b), using an ``and'' instead of but.)} \normalsize

                \item[d.]   There is milk in the fridge BUT there are no eggs in the fridge. ~\\
                            \footnotesize \textit{(Hint: ``but'' isn't an operator, but can be changed to ``and''...)} \normalsize
                \item[e.]   There is milk in the fridge AND there is either cheese OR eggs in the fridge.
            \end{itemize}
        \newpage

        \item   
            \begin{itemize}
                \item[a.] ~\\
                    \begin{tabular}{ | p{1cm} | p{1cm} || p{2cm} | p{3cm} | }
                        \hline
                        $p$ & $q$ & $\neg q$ & $p \land \neg q$ \\ \hline
                        T & T & F & F \\ \hline
                        T & F & T & T \\ \hline
                        F & T & F & F \\ \hline
                        F & F & T & F \\ \hline
                    \end{tabular} ~\\
                    
                \item[b.] ~\\
                    \begin{tabular}{ | p{1cm} | p{1cm} || p{2cm} | p{2cm} | p{3cm} | }
                        \hline
                        $p$ & $q$ & $\neg p$ & $\neg q$ & $\neg p \lor \neg q$ \\ \hline
                        T & T & F & F & F \\ \hline
                        T & F & F & T & T \\ \hline
                        F & T & T & F & T \\ \hline
                        F & F & T & T & T \\ \hline
                    \end{tabular}
            \end{itemize}
        \item   ~\\
            \begin{tabular}{ | p{2cm} | p{2cm} | p{2cm} | }
                \hline
                $p$ & $q$ & $r$   \\ \hline
                T & T & T         \\ \hline
                T & T & F         \\ \hline
                T & F & T         \\ \hline
                T & F & F         \\ \hline
                F & T & T         \\ \hline
                F & T & F         \\ \hline
                F & F & T         \\ \hline
                F & F & F         \\ \hline
            \end{tabular}
        \item   ~\\
            %   p       q       !p              !q          p or q          !p and !q                   !(p or q)
            \begin{tabular}
                { c     | c     || c            | c         || c            || c                        || c         }
                $p$     & $q$   & $\neg p$      & $\neg q$  & $(p \lor q)$  & $ \neg p \land \neg q $   & $ \neg ( p \lor q ) $     \\ \hline
                T       & T     & F             & F         & T             & F                         & F                        \\
                T       & F     & F             & T         & T             & F                         & F                        \\ 
                F       & T     & T             & F         & T             & F                         & F                        \\
                F       & F     & T             & T         & F             & T                         & T                        \\
            \end{tabular}

        \item
            \begin{itemize}
                \item[a.]   $(m \land \neg c) \lor (\neg m \land c)$
                \item[b.]   ~\\
                    \begin{tabular}{c | c | | c  }
                        & & Your statement from (a)... (write in) \\
                        $m$ & $c$ & $(m \land \neg c) \lor (\neg m \land c)$ \\ \hline
                        T & T & F \\ \hline
                        T & F & T \\ \hline
                        F & T & T \\ \hline
                        F & F & F
                    \end{tabular}
            \end{itemize}
    \end{enumerate}

}

\input{BASE-FOOT}
