\input{BASE-HEAD}
\newcommand{\laClass}       {Discrete Structures 1}
\newcommand{\laSemester}    {}
\newcommand{\laChapter}     {Proofs: Introduction}
\newcommand{\laType}        {exercise}
\newcommand{\laPoints}      {5}
\newcommand{\laTitle}       {Proofs: Introduction}
\newcommand{\laDate}        {}
\setcounter{chapter}{1}
\setcounter{section}{1}
\addtocounter{section}{-1}
\newcounter{question}

\toggletrue{answerkey}
%\togglefalse{answerkey}

\input{BASE-HEADER}

\input{BASE-INSTRUCTIONS-EXERCISE}

\notonkey{
% ASSIGNMENT ------------------------------------ %

    \section*{\laTitle}

    \subsection*{Turning statements to implications}

        \begin{intro}{Implications} ~\\
            This time we're exploring mathematical writing and getting introduced
            to proofs. To work with a statement, we turn it into an implication that
            we can work with mathematically.

            \paragraph{Example:}
            For every positive even integer $n$, $n+1$ is odd.

            Changing to an ``if, then" statement, we can form:

            If a positive integer $n$ is even, then $n+1$ is odd.
        \end{intro}

    \newpage
        % - QUESTION --------------------------------------------------%
        \stepcounter{question}
        \begin{questionNOGRADE}{\thequestion}
            Rewrite the following statements as ``if, then" statements.
            They don't need to be \textit{true} statements, we will talk
            about disproving statements next.

            \begin{enumerate}
                \item[a.]   When a positive integer $n$ is odd, then $n+1$ is even.
                            \vspace{3cm}

                \item[b.]   All squares have four equal sides.
                            \begin{hint}{Hint}
                                ``If $s$ is a square...''
                            \end{hint}
                            \vspace{4cm}

                \item[c.]   All prime numbers are odd.
                            \begin{hint}{Hint}
                                Remember that a proposition or implication
                                doesn't necessarily have to be the truth;
                                we can determine if it is actually a true or false
                                statement later on.
                            \end{hint}
                            \vspace{3cm}

            \end{enumerate}
        \end{questionNOGRADE}

    \newpage

    %------------------------------------------------------------------%
    \section*{Counter-examples}

        \begin{intro}{Counter-examples} ~\\
            A \textbf{counter-example} is a way to disprove a proposition.
            For implications, if we can keep the \textbf{hypothesis} true,
            and find a scenario where the \textbf{conclusion} ends up being false,
            then we can disprove a statement.

            \paragraph{Example:}
                \textit{``If $n$ is a prime number, then $n$ is odd.''}

            Hypothesis: $n$ is a prime number.

            Conclusion: $n$ is odd.

            ~\\
            Can we find any examples of prime numbers that aren't odd?
            We would need something where the hypothesis is true ($n$ is a prime number)
            and the conclusion is false ($n$ is \underline{not} odd.)

            ~\\
            So yes -- 2 is a prime number, and it is not odd. So this statement is false.
            We have disproven it with a counter-example.
        \end{intro}

        % - QUESTION --------------------------------------------------%
        \stepcounter{question}
        \begin{questionNOGRADE}{\thequestion}
            Disprove the following statements by coming up with a counter-example:

            \begin{enumerate}
                \item[a.]   For every even integer $n$, $n+1$ is also even.
                            \vspace{3cm}

                \item[b.]   If $n$ is a non-negative integer, then $n! > n$, where $n!$ is $n$-factorial.
                            \vspace{2cm}

            \end{enumerate}
        \end{questionNOGRADE}

    \newpage

    %------------------------------------------------------------------%
    \section*{Even, Odd, and Divisibility}

        \begin{intro}{\ }
            In Chapter 2, we will be working with the concept of even and
            odd numbers a lot, and working out proofs relating to these concepts.
            But, how do you actually specify that some number is even or odd symbolically?

            \begin{center}
                ``An integer is even if it is evenly divisible by two\\ and odd if it is not even."

                [...]

                ``A formal definition of an even number is that it is an integer of the form $n = 2k$,
                where $k$ is an integer;
                it can then be shown that an odd number is an integer of the form $n = 2k + 1$."
                \footnote{From https://en.wikipedia.org/wiki/Parity\_(mathematics)}
            \end{center}

        \end{intro}


        % - QUESTION --------------------------------------------------%
        \stepcounter{question}
        \begin{questionNOGRADE}{\thequestion}
            Identify the following numbers as either even or odd, by writing it as either
            2 times some other integer, or 2 times some other integer plus 1.

            \paragraph{Example:}    $21 = 2 \cdot 10 + 1$

            \begin{enumerate}
                \item[a.] 7
                    \iftoggle{answerkey}{ \begin{answer} $15 = 2 \cdot 3 + 1$ \end{answer} } { { ~\\ \raisebox{0pt}[0.7cm][0pt]{  } } }

                \item[b.] 9
                    \iftoggle{answerkey}{ \begin{answer} $15 = 2 \cdot 4 + 1$ \end{answer} } { { ~\\ \raisebox{0pt}[0.7cm][0pt]{  } } }

                \item[c.] 15
                    \iftoggle{answerkey}{ \begin{answer} $15 = 2 \cdot 7 + 1$ \end{answer} } { { ~\\ \raisebox{0pt}[0.7cm][0pt]{  } } }

                \item[d.] 8
                    \iftoggle{answerkey}{ \begin{answer} $8 = 2 \cdot 4$ \end{answer} } { { ~\\ \raisebox{0pt}[0.7cm][0pt]{  } } }

                \item[e.] 16
                    \iftoggle{answerkey}{ \begin{answer} $16 = 2 \cdot 8$ \end{answer} } { { ~\\ \raisebox{0pt}[0.7cm][0pt]{  } } }

                \item[f.] 20
                    \iftoggle{answerkey}{ \begin{answer} $20 = 2 \cdot 10$ \end{answer} } { { ~\\ \raisebox{0pt}[0.7cm][0pt]{  } } }

            \end{enumerate}
        \end{questionNOGRADE}


    %------------------------------------------------------------------%

        \begin{intro}{Divisibility}
            We looked at the definitions for an even and odd number.
            Here's one more - divisibility!

            ~\\
            An integer $n$ is divisible by $4$ if it is the result of
            4 times some other integer. Symbolically, $n = 4k$.

            ~\\
            We can use this definition for divisible by any number, as we
            need.
        \end{intro}

        \stepcounter{question}
        \begin{questionNOGRADE}{\thequestion}
            Using the definitions of even, odd, and divisible by \textit{some integer},
            prove that the following statements are true.

            \begin{itemize}
                \item   Even items must be of the form 2(some integer).
                \item   Odd items must be of the form 2(some integer) + 1.
                \item   Items divisible by $k$ must be of the form $k$(some integer).
            \end{itemize}

            \paragraph{Example:} ``12 is even" - rewrite as 2(6).

            \begin{enumerate}
                \item[a.]   100 is even.                            \vspace{0.5cm}

                \item[b.]   13 is odd.                              \vspace{0.5cm}

                \item[c.]   -13 is odd.                             \vspace{0.5cm}

                \item[d.]   20 is divisible by 5.                   \vspace{0.5cm}

                \item[e.]   20 is divisible by 4.                   \vspace{0.5cm}

                \item[f.]   $6n$ is even.                           \vspace{0.5cm}

                \item[g.]   $8n^{2} + 8n + 4$ is divisible by 4.    \vspace{0.5cm}

            \end{enumerate}
        \end{questionNOGRADE}

    \newpage
    %------------------------------------------------------------------%
    \section*{Closure properties of integers}

        \begin{intro}{\ }
            The set of all integers is written as $\mathbb{Z}$.

            \begin{center}
                \textit{``A set has closure under an operation if
                performance of that operation on members of the set
                always produces a member of the same set; in this
                case we also say that the set is closed under the operation."}
                \footnote{From https://en.wikipedia.org/wiki/Closure\_(mathematics)}
            \end{center}

            \begin{itemize}
                \item   If you add two integers, the result is also an integer
                \item   If you subtract two integers, the result is also an integer
                \item   If you multiply two integers, the result is also an integer
                \item   If you divide two integers, the result \textbf{may not be an integer}
            \end{itemize}

            We can use these properties in our proofs, by remembering that
            if two integers $k$ and $j$ are added, the result $k+j$ is also an integer.
        \end{intro}


        \stepcounter{question}
        \begin{questionNOGRADE}{\thequestion}
            Identify if the result of the following operations belong to the set of integers $\mathbb{Z}$.
            Write the result as either $\in \mathbb{Z}$ or $\not\in \mathbb{Z}$.

            \begin{enumerate}
                \item[a.]   2 + 8
                            \vspace{0.7cm}
                \item[b.]   12 - 4
                            \vspace{0.7cm}
                \item[c.]   5 * 3
                            \vspace{0.7cm}
                \item[d.]   6 / 3
                            \vspace{0.7cm}
                \item[e.]   5 / 2
                            \vspace{0.7cm}
            \end{enumerate}
        \end{questionNOGRADE}

    \newpage
    %------------------------------------------------------------------%
    \section*{Types of Proofs}

        \begin{intro}{Direct Proof} ~\\
            \textbf{Example proof:}
            ``The result of summing any odd integer with any even integer is an odd integer."

            We should first write this in mathematical language. Let's define our numbers:
            an even number and an odd number. But how do we write these symbolically?

            \begin{center}
                \begin{tabular}{c c}
                    odd integer & even integer
                    \\
                    $x = 2k+1$ & $y = 2j$
                \end{tabular}
            \end{center}
            ~\\
            An even integer is technically a number that is \textbf{evenly divisible by 2}.
            If we re-word this, we can say that an even integer is ``some \textit{other} integer
            times 2." (Note here that $y$ is our even integer, and it is 2 times some other integer $j$.)
            ~\\~\\
            We know what an even integer is... and an odd integer is just one more than an even number.
            In this case, we define our odd integer $x$, as 2 times \textit{some other integer} $k$, plus 1.
            (Again note that we're using a different variable for $x$! $j$ for $y$ and $k$ for $x$...
            Make sure to not re-use the same variable.)
            ~\\~\\
            With these numbers defined, we can start working mathematically, translating our original
            statement into symbols.

            \begin{center}
                \begin{tabular}{l l}
                    $x + y$         &       An odd number plus an even number...
                    \\
                    $x + y = (2k+1) + (2j) $    &   Adding definitions of odd and even...
                    \\
                    $x + y = 2(k + j) + 1 $     &   Rewriting to the definition of an odd \#...
                \end{tabular}
            \end{center}

            $(k+j)$ is still an integer, and the definition of an odd integer
            is 2 times \textit{some integer} plus 1, proving our statement.

        \end{intro}

    \newpage
        \stepcounter{question}
        \begin{questionNOGRADE}{\thequestion}
            Prove the following statements.

            \begin{enumerate}
                \item[a.] For all integers $n > 0$, if $n$ is even, then $n^{2}$ is also even.

                    \begin{hint}{Hint}
                        Begin with n is even, $n = 2k$, then simplifying
                        $2$ to try to get the definition
                        of an even number, 2 $\times$ some integer.
                    \end{hint}

                    \vspace{5cm}

                \item[b.] For all integers $n > 0$, if $n$ is odd, then $n^{2} + n$ is even.

                    \begin{hint}{Hint}
                        Begin with n is odd, $n = 2k+1$, then simplifying
                        $(2k+1)^{2} + (2k+1)$ to try to get the definition
                        of an even number, 2 $\times$ some integer.
                    \end{hint}


            \end{enumerate}
        \end{questionNOGRADE}

}
{
% KEY ------------------------------------ %

    \begin{enumerate}
        \item
            \begin{itemize}
                \item[a.]   If $n$ is odd, then $n+1$ is even.
                \item[b.]   If $s$ is a square, then $s$ has four equal sides.
                \item[c.]   If $p$ is a prime nummber, then $p$ is odd.
            \end{itemize}
            
        \item
            \begin{itemize}
                \item[a.]   $2$ is even but $2+1 = 3$ is not even.
                \item[b.]   $0!$ is 1.
            \end{itemize}
            
        \item
            \begin{itemize}
                \item[a.]   7 = 2(3) + 1
                \item[b.]   9 = 2(8) + 1
                \item[c.]   15 = 2(7) + 1
                \item[d.]   8 = 2(4)
                \item[e.]   16 = 2(8)
                \item[f.]   20 = 2(10)
            \end{itemize}
            
        \item
            \begin{itemize}
                \item[a.]   100 = 2(50)
                \item[b.]   13 = 2(6) + 1
                \item[c.]   -13 = 2(7) + 1
                \item[d.]   20 = 5(4)
                \item[e.]   20 = 4(5)
                \item[f.]   $6n = 2(3n)$
                \item[g.]   $8n^{2} + 8n + 4 = 4(2n^{2} + 2n + 1)$
            \end{itemize}
            
        \item
            \begin{itemize}
                \item[a.]   $2 + 8 \in \mathbb{Z}$
                \item[a.]   $12 - 4 \in \mathbb{Z}$
                \item[a.]   $5 * 3 \in \mathbb{Z}$
                \item[a.]   $6 / 3 \in \mathbb{Z}$
                \item[a.]   $5 / 2 \not\in \mathbb{Z}$
            \end{itemize}

        \newpage
        \item
            \begin{itemize}
                \item[a.]   \textbf{For all integers $n > 0$, if $n$ is even, then $n^{2}$ is also even.} ~\\ ~\\
                            $n$ is an even integer, so begin with $n = 2k$. ~\\ ~\\
                            We are trying to prove that $n^{2}$ is also even.
                            Let's plug in our equation for $n$. ~\\ ~\\
                            $n^{2}$ rewrite as: $= (2k)^{2}$ ~\\
                            Simplify: $= 4k^{2}$ ~\\
                            Factor: $= 2(2k^{2})$ ~\\
                            \begin{framed}Proof: $2(2k^{2})$ is in the form of the definition of an even integer. Therefore,
                            we have proven the statement.\end{framed}
                            ~\\~\\

                \item[b.]   \textbf{For all integers $n > 0$, if $n$ is odd, then $n^{2} + n$ is even.} ~\\ ~\\
                            $n$ is an odd integer, so $n = 2k+1$. ~\\

                            We are trying to prove that $n^{2} + n$ is even. Substitute our equation for $n$. ~\\

                            $n^{2} + n$ rewrite as: $= (2k+1)^{2} + (2k+1)$ ~\\
                            FOIL out: $4k^{2} + 4k + 1 + 2k + 1$ ~\\
                            Simplify: $4k^{2} + 6k + 2$ ~\\
                            Factor: $2(2k^{2} + 3k + 1)$ ~\\
                            \begin{framed}Proof: $2(2k^{2} + 3k + 1)$ is in the form of the definition of an even integer. Therefore,
                            we have proven the statement.\end{framed}

            \end{itemize}
    \end{enumerate}

}

\input{BASE-FOOT}
