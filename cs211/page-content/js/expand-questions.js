$( document ).ready( function() {
    $( ".qa-question" ).click( function() {
        var nextAnswer = $( this ).next( ".qa-answer" );

        if ( nextAnswer.css( "display" ) == "none" )
        {
            // Show
            $( this ).next( ".qa-answer" ).slideDown( "fast" );
        }
        else
        {
            // Hide
            $( this ).next( ".qa-answer" ).slideUp( "fast" );
        }
    } );
} );
