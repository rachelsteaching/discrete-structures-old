\input{BASE-HEAD}
\newcommand{\laClass}       {CS 211}
\newcommand{\laSemester}    {Spring 2018}
\newcommand{\laChapter}     {5.1}
\newcommand{\laType}        {Exercise}
\newcommand{\laPoints}      {5}
\newcommand{\laTitle}       {Intro to Combinatorics}
\newcommand{\laDate}        {Jan 16, 2018}
\setcounter{chapter}{5}
\setcounter{section}{1}
\addtocounter{section}{-1}
\newcounter{question}

\toggletrue{answerkey}
\togglefalse{answerkey}

\input{BASE-HEADER}

\input{INSTRUCTIONS-EXERCISE}

    \section{\laTitle}

    Combinatorics are counting problems, asking how many different combinations
    or outcomes there can be given some input(s), and when selecting a certain
    amount of items. These problems differ in how you solve them based on
    the type of structure.

    \begin{intro}{Types of structures}
        \begin{center}
            \begin{tabular}{l | c | c | c }
                \textbf{}
                    & \textbf{Repeats allowed?}
                    & \textbf{Order matters?}
                \\ \hline
                Ordered list
                    & yes
                    & yes

                \\ \hline
                Unordered list
                    & yes
                    & no
                \\ \hline
                Permutations
                    & no
                    & yes
                \\ \hline
                Sets
                    & no
                    & no
            \end{tabular}
        \end{center}

        \paragraph{Examples:} ~\\
        \begin{itemize} \footnotesize
            \item   \textbf{How many ways are there to rearrange the letters ABC?} ~\\
                    When building a word (even a nonsensical one),
                    the \underline{order of the letters matter}; ``ABC'' is different
                    from ``CBA''. Also, if we're only rearranging these letters,
                    \underline{repeats are not allowed}, since we have a finite
                    supply of each letter. This is a \underline{Permutation}.
                    
            \item   \textbf{In an arcade game, there are 3 spaces for you to put your name in the high-score table.
                    Assuming only capital letters are allowed, how many ways can you make a name?} ~\\
                    For this one, we're pulling letters from the alphabet, and
                    there is no restrictions. We \underline{can have repeats}, ``AAA''. Since
                    we are still building a name (``SAM $\neq$ AMS''), \underline{order matters}.
                    This is a \underline{Ordered list}.
        \end{itemize}
        
    \end{intro}

    \newpage
    \begin{intro}{Examples (continued)}
        \begin{itemize} \footnotesize
            \item   \textbf{You're flipping a coin 5 times. In how many ways can you get more HEADS than TAILS?} ~\\
                    For this problem, we \underline{don't care about the order} - just how many heads/tails we have.
                    So, essentially ``HHHTT'' and ``TTHHH'' would be seen as the same outcome. Also, since we're
                    flipping a coin and either getting Heads or Tails, each time we flip we \underline{can get a repeat}.
                    Therefore, this is a \underline{Unordered list}.

            \item   \textbf{You're going to build a committee of 3 people out of a class of 20. How many ways can a committee be built?} ~\\
                    For this one, we are electing committee members so every role is the same. We can think of this as
                    \underline{order not mattering}, since it doesn't matter if you're committee member \#1, \#2, or \#3.
                    When we're choosing people to be in the committee, we \underline{cannot have duplicates} --
                    for this type of question, it is assumed that a person can only fill one role. Once they've been
                    selected, they're out of the selection pool. Therefore, this is a \underline{Set}.
        \end{itemize}
    \end{intro}

    \newpage

        For questions 1 through 4, answer yes/no for ``Repetitions allowed?''
        and ``Order matter?'', and then identify the structure.
        You DON'T need to solve the problems.

        
    \stepcounter{question}
    \begin{questionNOGRADE}{\thequestion}
        \begin{wrapfigure}{r}{0.3\textwidth}\centering
            \includegraphics[scale=0.6]{images/51table.png}
        \end{wrapfigure}

        You want to throw a birthday party at your favorite restaurant,
        but you can only book 7 other seats. (You're going to take the 8th seat,
        but don't count it in this problem.) You have 11 friends to choose from.
        How many different groups of friends could attend your birthday party?

        ~\\~\\
        \begin{tabular}{p{4cm} p{2cm} p{2cm}}
            Repetitions allowed? & \Circle\ Yes & \Circle\ No \\
            Order matters? & \Circle\ Yes & \Circle\ No \\
            What structure?
        \end{tabular}
    \end{questionNOGRADE}
    
    \vspace{1cm}
    
    \hrulefill
    
    \stepcounter{question}
    \begin{questionNOGRADE}{\thequestion}
        \begin{wrapfigure}{r}{0.3\textwidth}\centering
            \includegraphics[scale=0.7]{images/51chips.png}
        \end{wrapfigure}
        There are only 4 bags of chips left at the sandwich shop.
        After a bag is taken, it cannot be selected again.
        Nacho, ranch, garden, and potato. You and your friend both
        want chips. If you're selecting first for your friend
        and then for yourself, how many ways are there to get chips?
        
        ~\\~\\
        \begin{tabular}{p{4cm} p{2cm} p{2cm}}
            Repetitions allowed? & \Circle\ Yes & \Circle\ No \\
            Order matters? & \Circle\ Yes & \Circle\ No \\
            What structure?
        \end{tabular}
    \end{questionNOGRADE}

    \newpage
    \stepcounter{question}
    \begin{questionNOGRADE}{\thequestion}
        \begin{wrapfigure}{r}{0.3\textwidth}\centering
            \includegraphics[scale=0.9]{images/51pens.png}
        \end{wrapfigure}
        A student is purchasing colored pens to use for each of their four classes.
        Assume that the student will not use the same color for two classes;
        each class gets one color.
        First they choose a pen for Discrete Math, then for English, then for
        Physics, and then for Programming. They can choose from the colors
        red, orange, yellow, green, blue, or purple.
        
        ~\\~\\
        \begin{tabular}{p{4cm} p{2cm} p{2cm}}
            Repetitions allowed? & \Circle\ Yes & \Circle\ No \\
            Order matters? & \Circle\ Yes & \Circle\ No \\
            What structure?
        \end{tabular}
    \end{questionNOGRADE}
    
    \vspace{1cm}
    
    \hrulefill
    
    \stepcounter{question}
    \begin{questionNOGRADE}{\thequestion}
        \begin{wrapfigure}{r}{0.2\textwidth}\centering
            \includegraphics[scale=0.5]{images/51-food.png}
        \end{wrapfigure}
        At Macaque Express, you can order two entrees for a normal sized meal.
        You can choose one portion of two different items as your two entrees,
        or you can choose two portions of one item as your two entress.
        If there are 13 entree options available, how many meals can you build?
        
        ~\\~\\
        \begin{tabular}{p{4cm} p{2cm} p{2cm}}
            Repetitions allowed? & \Circle\ Yes & \Circle\ No \\
            Order matters? & \Circle\ Yes & \Circle\ No \\
            What structure?
        \end{tabular}
    \end{questionNOGRADE}




    \newpage
    
    \stepcounter{question}
    \begin{questionNOGRADE}{\thequestion} \footnotesize

        We have a pool of four people, Andrew, Bob, Carly, Diane, and two prizes will be distributed
        between two of these people.
        \footnote{Based on Practice Problem 2 from Discrete Mathematics, Ensley \& Crawley}

        \begin{enumerate}
            \item[a.] Assuming that prize \#1 and prize \#2 are different, list out
            all the ways the prizes can be distributed (i.e., permutations).

            \textit{Example: (A, A), (A, B), (B, A), ...}
            \vspace{2cm}

            \item[b.] Assuming that prize \#1 and price \#2 are both the same,
            list out all the ways the prizes can be distributed (i.e., sets).

            \textit{Note: \{A, B\} and \{B, A\} would be considered the same.}
            \vspace{2cm}
        \end{enumerate}
        
    \end{questionNOGRADE}

    \newpage

    \stepcounter{question}
    \begin{questionNOGRADE}{\thequestion}

        All possible outcomes of rolling a red 6-sided die and a green 6-sided die,
        in an organized way is given in the table. For the given set of data, answer the questions.
        \footnote{Based on Exercise 5.1 \#6 from Discrete Mathematics, Ensley \& Crawley}
        ~\\

        \begin{tabular}{ l | c c c c c c }
            & Green 1 & Green 2 & Green 3 & Green 4 & Green 5 & Green 6 \\ \hline
            Red 1 & (1,1) & (1,2) & (1,3) & (1,4) & (1,5) & (1,6) \\
            Red 2 & (2,1) & (2,2) & (2,3) & (2,4) & (2,5) & (2,6) \\
            Red 3 & (3,1) & (3,2) & (3,3) & (3,4) & (3,5) & (3,6) \\
            Red 4 & (4,1) & (4,2) & (4,3) & (4,4) & (4,5) & (4,6) \\
            Red 5 & (5,1) & (5,2) & (5,3) & (5,4) & (5,5) & (5,6) \\
            Red 6 & (6,1) & (6,2) & (6,3) & (6,4) & (6,5) & (6,6) \\
            
        \end{tabular}

        \begin{enumerate}
            \item[a.]   Of the listed outcomes, how many are ``doubles''
                        (both dice have the same value)? What percentage of
                        total outcomes does this represent?
                        (You can write it as a fraction)
                        \vspace{2cm}

            \item[b.]   How many are doubles \textit{and} have the sum
                        of two dice values less than 4?
                        (You can write it as a fraction)
                        \vspace{2cm}

            \item[c.]   How many have a 5 on exactly one of the dice?
                        \vspace{2cm}

            \item[d.]   How many have a 5 on at least one of the dice?
                        \vspace{2cm}
        \end{enumerate}
        
    \end{questionNOGRADE}

    \newpage

    \stepcounter{question}
    \begin{questionNOGRADE}{\thequestion}

        Identify all possible outcomes for the following:

        \begin{enumerate}
            \item[a.]   All possible outcomes if you flip one coin.
                        \vspace{2cm}
            
            \item[b.]   All possible outcomes if you flip two coins together.
                        \vspace{2cm}
            
            \item[c.]   All possible outcomes if you flip three coins together.
                        \vspace{2cm}
        \end{enumerate}
        
    \end{questionNOGRADE}

    \newpage

    \stepcounter{question}
    \begin{questionNOGRADE}{\thequestion} \footnotesize

        The following tree diagrams all possible outcomes when tossing a penny,
        a nickel, and a dime together. Given this tree, answer the following questions
        \footnote{Based on Exercise 5.1 \#8 from Discrete Mathematics, Ensley \& Crawley}

        \begin{center}
            \begin{tikzpicture}
                \coordinate (Root) at (0,4);
                \coordinate (L) at (-4,3);
                \coordinate (R) at (4,3);
                \coordinate (LL) at (-6,2);
                \coordinate (LR) at (-2,2);
                \coordinate (RR) at (6,2);
                \coordinate (RL) at (2,2);
                \coordinate (LLL) at (-7,1);
                \coordinate (LLR) at (-5,1);
                \coordinate (LRL) at (-3,1);
                \coordinate (LRR) at (-1,1);
                \coordinate (RRR) at (7,1);
                \coordinate (RRL) at (5,1);
                \coordinate (RLR) at (3,1);
                \coordinate (RLL) at (1,1);
                
                \draw (LLL) -- (LL);     \node at (-7,0) {TTT};
                \draw (LLR) -- (LL);     \node at (-5,0) {TTH};
                \draw (LRL) -- (LR);     \node at (-3,0) {THT};
                \draw (LRR) -- (LR);     \node at (-1,0) {THH};
                
                \draw (LL) -- (L);
                \draw (LR) -- (L);
                
                \draw (L) -- (Root);
                
                \draw (RLL) -- (RL);     \node at (1,0) {HTT};
                \draw (RLR) -- (RL);     \node at (3,0) {HTH};
                \draw (RRL) -- (RR);     \node at (5,0) {HHT};
                \draw (RRR) -- (RR);     \node at (7,0) {HHH};
                
                \node[color=red] at (-5.5,3) {Penny flip};
                \node[color=blue] at (-7,2) {Nickel};
                \node[color=purple] at (-8,1) {Dime};
                
                \draw (6,2) -- (4,3);
                \draw (2,2) -- (4,3);
                
                \draw (4,3) -- (0,4);
                
                \filldraw[fill=white] (Root) circle (10pt) node{-};
                
                \filldraw[red,fill=white] (L) circle (10pt) node{T};
                \filldraw[red,fill=white] (R) circle (10pt) node{H};
                
                \filldraw[blue,fill=white] (LL) circle (10pt) node{T};
                \filldraw[blue,fill=white] (LR) circle (10pt) node{H};
                
                \filldraw[blue,fill=white] (RR) circle (10pt) node{H};
                \filldraw[blue,fill=white] (RL) circle (10pt) node{T};
                
                \filldraw[purple,fill=white] (LLL) circle (10pt) node{T};
                \filldraw[purple,fill=white] (LLR) circle (10pt) node{H};
                
                \filldraw[purple,fill=white] (LRL) circle (10pt) node{T};
                \filldraw[purple,fill=white] (LRR) circle (10pt) node{H};
                
                \filldraw[purple,fill=white] (RRR) circle (10pt) node{H};
                \filldraw[purple,fill=white] (RRL) circle (10pt) node{T};
                
                \filldraw[purple,fill=white] (RLR) circle (10pt) node{H};
                \filldraw[purple,fill=white] (RLL) circle (10pt) node{T};
            \end{tikzpicture}
        \end{center}

        \begin{enumerate}
                \item[a.]   Of the outcomes shown, for how many does
                            the result on the penny match the result on the dime?
                        \vspace{1.5cm}

                \item[b.]   Which is more likely, that the result on the penny
                            matches the result on the dime, or that they do not match?
                        \vspace{1.5cm}

                \item[c.]   For how many of the outcomes do all three coins match?
                        \vspace{1.5cm}

                \item[d.]   For how many of the outcomes do exactly two of the coins match?
                        \vspace{1.5cm}

                \item[e.]   For how many outcomes is the number of tails less than the number of heads?
                        \vspace{1.5cm}
        \end{enumerate}
        
    \end{questionNOGRADE}

    

\input{BASE-FOOT}
